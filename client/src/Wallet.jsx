import server from "./server";
import * as secp from "ethereum-cryptography/secp256k1";
import { toHex, hexToBytes } from "ethereum-cryptography/utils";
import { useEffect } from "react";

function Wallet({
  address,
  setAddress,
  balance,
  setBalance,
  signature,
  setSignature,
  messageHash,
  setMessageHash,
  recoveryBit,
  setRecoveryBit,
}) {
  async function handleFormSubmit(evt) {
    evt.preventDefault();

    const publicKey = secp.recoverPublicKey(
      hexToBytes(messageHash),
      signature,
      recoveryBit
    );

    setAddress(toHex(publicKey));
  }

  useEffect(() => {
    async function getBalance() {
      if (address) {
        console.log("send request");
        const {
          data: { balance },
        } = await server.get(`balance/${address}`);
        setBalance(balance);
      } else {
        setBalance(0);
      }
    }

    getBalance();
  }, [address]);

  return (
    <form className="container wallet" onSubmit={handleFormSubmit}>
      <h1>Your Wallet</h1>

      <label>
        Message hash
        <input
          placeholder="Must be in hex"
          value={messageHash}
          onChange={(e) => setMessageHash(e.target.value)}
        ></input>
      </label>

      <label>
        Signature
        <input
          placeholder="Must be in hex"
          value={signature}
          onChange={(e) => setSignature(e.target.value)}
        ></input>
      </label>

      <label>
        Recovery bit
        <input
          type="number"
          placeholder="Must be in hex"
          value={recoveryBit}
          onChange={(e) => setRecoveryBit(parseInt(e.target.value))}
        ></input>
      </label>

      <input type="submit" className="button" value="Balance Check" />

      <div>Address: {address.slice(0, 10)}...</div>

      <div className="balance">Balance: {balance}</div>
    </form>
  );
}

export default Wallet;
