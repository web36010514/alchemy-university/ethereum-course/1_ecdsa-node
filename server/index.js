const express = require("express");
const app = express();
const cors = require("cors");
const port = 3042;

app.use(cors());
app.use(express.json());

const balances = {
  "0481667c4cef3c4d6a21f154b0acdb0cba1f25b0ef581313bbb83a632cceeb9cf484cca0024e3b1ea33a82d34a0bdb7816e16c38f821e86a451bf73e84fe8f499d": 100, //dan
  "043441e116ec3e9c1a2180b7ced55494a49962a59170dd8638b2e8fe691965e2c4a4a83893cab0169427c53343b15e84dbe7886327262a3cf3fbfcf385501bbcb2": 50, //ben
  "0459a140347aa97e7660045e7573de2bd17fa30eda418945d2e74780eafa781fe8a4f7b511a9393616d690d467982e3e2c0e03ed1e918d6618ffdc77cdf12b4736": 75, //al
};

app.get("/balance/:address", (req, res) => {
  const { address } = req.params;
  console.log(address);
  const balance = balances[address] || 0;
  res.send({ balance });
});

app.post("/send", (req, res) => {
  //TODO: get a signature from the client-side application
  //recover the public address from the signature

  const { sender, recipient, amount } = req.body;

  setInitialBalance(sender);
  setInitialBalance(recipient);

  if (balances[sender] < amount) {
    res.status(400).send({ message: "Not enough funds!" });
  } else {
    balances[sender] -= amount;
    balances[recipient] += amount;
    res.send({ balance: balances[sender] });
  }
});

app.listen(port, () => {
  console.log(`Listening on port ${port}!`);
});

function setInitialBalance(address) {
  if (!balances[address]) {
    balances[address] = 0;
  }
}
