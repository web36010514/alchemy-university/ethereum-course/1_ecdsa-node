const secp = require("ethereum-cryptography/secp256k1");
const { toHex } = require("ethereum-cryptography/utils");

const { keccak256 } = require("ethereum-cryptography/keccak");
const { utf8ToBytes } = require("ethereum-cryptography/utils");

const privateKey = secp.utils.randomPrivateKey();

console.log("privateKey: ", toHex(privateKey));

const publicKey = secp.getPublicKey(privateKey);

console.log("publicKey: ", toHex(publicKey));

async function signMessage(msg, privateKey) {
  const hash = keccak256(utf8ToBytes(msg));
  console.log("hash:", toHex(hash));

  const signature = await secp.sign(hash, privateKey, { recovered: true });
  const [sig, recoveryBit] = signature;
  console.log("sig", toHex(sig));
  console.log("recoveryBit", recoveryBit);

  const publicKey = secp.recoverPublicKey(toHex(hash), sig, recoveryBit);
  console.log("publicKey: ", toHex(publicKey));

  return signature;
}

const msg = "hello";

signMessage(msg, privateKey);
